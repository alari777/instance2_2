require('dotenv').config();
const ip = require('ip');

console.log('IP', ip.address());

const Connections = require('./connections');

const connections = new Connections();

const Preparation = require('./preparation');

const preparation = new Preparation(connections.redisclients.redismain, connections.postgre);

const Settings = require('./settings');

const settings = new Settings();

const Binancekeeping = require('./binance-keeping');

const binancekeeping = new Binancekeeping(connections, settings.generall);

const Binancecalcs = require('./binance-calcs');

const Market = require('./market');
const Algorithmsbuy = require('./market/algorithms/buy');
const Actionsbuy = require('./market/actions/buy');
const Algorithmssell = require('./market/algorithms/sell');
const Actionssell = require('./market/actions/sell');

// const Users = require('./users');

// const users = new Users(connections);
// users.main();

// const { performance } = require('perf_hooks');
// const log = require('./log')(module);

let markets = [];
let counter = 0;
let counterVar = 2000;
if (process.env.MONITORINGSBINANCE_TEST1 === 'monitoringsbinance_mar_total_sim_test5') {
  counterVar = 500;
  console.log('counter1', counter);
}

const marketsTop = ['ETHBTC', 'IOTABTC', 'BCHABCBTC', 'XRPBTC', 'EOSBTC', 'ADABTC', 'LTCBTC', 'NEOBTC', 'BATBTC', 'RVNBTC', 'TRXBTC', 'NANOBTC', 'IOSTBTC', 'ONTBTC', 'XMRBTC', 'XLMBTC', 'ZECBTC', 'WAVESBTC', 'DASHBTC', 'QTUMBTC', 'OMGBTC', 'VETBTC'];
// const postgreone = connections.connectToPostgre();

function calculatingBinanceDatas() {
  Object.keys(connections.redissubs).forEach((key) => {
    connections.redissubs[key].subscribe('depthcachesymbol', () => {
      connections.redissubs[key].on('message', async (channel, message) => {
        // if (key === 'redismain') console.log('key', key, process.env.MONITORINGSBINANCE_TEST1);
        if (key !== 'redismain' || (key === 'redismain' && process.env.MONITORINGSBINANCE_TEST1 === 'monitoringsbinance_mar_total_sim_test5')) {
          const arr = JSON.parse(message);
          const dataset = arr[0]; // message;
          if (markets.includes(dataset) && channel === 'depthcachesymbol') {
            try {
              // console.log(dataset, key, channel);
              const binancecalcs = new Binancecalcs(connections, settings.generall, dataset, connections.redisclients[key], arr[1], arr[2]);
              await binancecalcs.setWorkingBidsAndAsksObjsNew();
              await binancecalcs.makeWorkingBidsAndAsksObjs();

              const market = new Market(connections, binancecalcs, dataset);
              // Роутер на определение монеты: на покупке или на продаже она находится
              const typetrd = await market.router();

              switch (typetrd) {
                case 'buy': {
                  const algobuy = new Algorithmsbuy(connections, binancecalcs, binancekeeping, dataset);
                  let algoresultbuy = null;
                  if (process.env.MONITORINGSBINANCE_TEST1 === 'monitoringsbinance_mar_total_sim_test4') {
                    algoresultbuy = await algobuy.mainNewAlgo();
                  } else {
                    algoresultbuy = await algobuy.main();
                  }
                  if (algoresultbuy === 'buy now') {
                    console.log(dataset, algoresultbuy);
                    // console.log(dataset, marketHistory);

                    const actionbuy = new Actionsbuy(connections, binancecalcs, algobuy, dataset);
                    let buyactionresult = '';

                    if (process.env.ROBOT_MODE === 'dev') {
                      await actionbuy.freezeBuyMarket('freeze');
                      buyactionresult = await actionbuy.buyOrderDev();
                    }

                    if (process.env.ROBOT_MODE === 'prod') {
                      await actionbuy.freezeBuyMarket('freeze');
                      buyactionresult = await actionbuy.buyOrderProd();
                    }

                    if (buyactionresult === 'buy success') {
                      console.log(dataset, buyactionresult);
                    }
                  }
                }
                  break;

                case 'sell': {
                  const algosell = new Algorithmssell(connections, binancecalcs, dataset);
                  let algoresultsell = null;
                  if (process.env.MONITORINGSBINANCE_TEST1 === 'monitoringsbinance_mar_total_sim_test4') {
                    algoresultsell = await algosell.mainNewAlgo();
                  } else {
                    algoresultsell = await algosell.main();
                  }
                  if (algoresultsell === 'sell now') {
                    const actionsell = new Actionssell(connections, binancecalcs, algosell, dataset);
                    let sellactionresult = '';
                    if (process.env.ROBOT_MODE === 'dev') {
                      await actionsell.freezeSellMarket('freeze');
                      sellactionresult = await actionsell.sellOrderDev();
                    }
                    if (process.env.ROBOT_MODE === 'prod') {
                      await actionsell.freezeSellMarket('freeze');
                      sellactionresult = await actionsell.sellOrderProd();
                    }

                    if (sellactionresult === 'sell success') {
                      console.log(dataset, sellactionresult);
                    }
                  }
                }
                  break;

                case 'freeze':
                  // console.log(dataset, typetrd);
                  break;

                default:
                  break;
              }

              counter += 1;
              if (counter === counterVar) {
                console.log('Postgre check generall connect. Counter Ticks:', counter);
                counter = 0;
                const sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} LIMIT 1;`;
                connections.postgre.query(sql, (err) => {
                  if (err) console.log(err);
                });
              }

              // console.log(dataset, typetrd);
            } catch (err) {
              // console.log(err);
            }
          } else {
            binancekeeping.setBuySellNow(dataset, arr[1], arr[2]);
          }
        }
      });
    });
  });
}


(async function robot() {
  // Подготовка данных. Получаем из PostGre монеты
  // Запихиваем актуальные данные по каждой монете в Redis
  try {
    const result = await preparation.getMarketsForWork();
    if (result === null) {
      console.log('No aviable markets for works');
      return false;
    }

    markets = result.step1.n;
    console.log('1.) Starting #1. Amount of CURRENT markets:', markets.length);
    console.log('2.) Starting #2.', result.step2);
    
    switch (process.env.MONITORINGSBINANCE_TEST1) {
      case 'monitoringsbinance_mar_total_sim_test3':
        markets = marketsTop;
        console.log('3.) Starting #3. Amount of PARENT markets:', markets.length);
        break;
      default:
        break;
    }

    if (process.env.MONITORINGSBINANCE_TEST1 === 'monitoringsbinance_mar_total_sim_test5') {
      // Запускаем Binance WebScokets для готовых к работе монет: ордера, маркет хистори
      binancekeeping.getBinanceOrders(markets);
    }
  } catch (err) {
    // console.log(err);
  }


  // Запускаем обработку данных и алгоритмы: находим стенки, застенки,
  // определяем какой статус у монеты, алгоритм проверки покупки или
  // продажи, принятие решение о покупке или продаже и т.п.
  calculatingBinanceDatas();

  return true;
}());
