// var winston = require('winston');

// class Log {
// 	constructor() {
// 		this.options = {
// 		  file: {
// 		  	filename: './debug.log', 
// 		  	level: 'debug'
// 		  },
// 		  console: {
// 		    timestamp: true, // function() { return new Date().toString() }
// 		    colorize: true,
// 		    json: false,
// 		    level: 'info',
// 		    handleExceptions: true  
// 		  },
// 		};
// 	}

// 	makeLogger(path) {
// 		if (path.match(/index.js$/)) {

// 			let transports = [
// 				new winston.transports.Console(this.options.console),
// 				new winston.transports.File(this.options.file))
// 			];

// 			return winston.createLogger({ transports: transports, exitOnError: false });
// 		} 
// 			else {
// 				return winston.createLogger({
// 					transports: []
// 				});
// 			}
// 	}	
// }

// module.exports = Log;

var winston = require('winston');

module.exports = function(module) {
  return makeLogger(module.filename);
};

function makeLogger(path) {
    let transports = [
      new winston.transports.Console({
	    timestamp: true,
	    colorize: true,
	    // json: false,
	    level: 'info'
	    // handleExceptions: true 
      }),

      new winston.transports.File({ filename: './log/debug.log', level: 'debug' })
    ];

    return winston.createLogger({ transports: transports });

}