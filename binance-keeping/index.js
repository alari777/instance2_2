class Binancekeeping {
  constructor(connection, settings) {
    this.redis = connection.redisclients.redismain;
    // this.redispub = connection.redispub;
    this.redissubkeep = connection.redissubkeep.redismain;
    this.redispubkeep = connection.redispubkeep.redismain;

    this.postgre = connection.postgre;
    this.binance = connection.binance;

    this.depthCacheBinance = settings.depthCacheBinance;
    this.lenBids = settings.lenBids;
    this.lenAsks = settings.lenAsks;

    this.bids = {};
    this.asks = {};

    this.postgreone = connection.connectToPostgre();

    return this;
  }

  getBinanceOrders(markets) {
    let counter = 0;
    this.binance.robot.websockets.depthCache(markets, (symbol, depth) => {
      const bids = this.binance.robot.sortBids(depth.bids);
      const asks = this.binance.robot.sortAsks(depth.asks);

      const lenBids = Object.keys(bids).length;
      const lenAsks = Object.keys(asks).length;

      // let firstBids = this.binance.robot.first(bids);
      // let firstAsks = this.binance.robot.first(asks);

      if (lenBids >= this.lenBids && lenAsks >= this.lenAsks) {
        this.redis.pipeline([
          ['set', `${symbol}:bids`, JSON.stringify(bids)],
          ['set', `${symbol}:asks`, JSON.stringify(asks)],
        ]).exec(() => {
          counter += 1;

          // this.redissubkeep.subscribe('depthcache', () => {
          //   this.redispubkeep.publish('depthcache', symbol);
          // });

          if (process.env.MONITORINGSBINANCE_TEST1 === 'monitoringsbinance_mar_total_sim_test5') {
            this.redissubkeep.subscribe('depthcachesymbol', () => {
              const arr = [];
              const newbids = bids;
              const objbids = {};
              const newasks = asks;
              const objasks = {};

              Object.keys(newbids).forEach((key, index) => {
                if (index <= 99) objbids[key] = newbids[key];
              });

              Object.keys(newasks).forEach((key, index) => {
                if (index <= 99) objasks[key] = newasks[key];
              });
              arr.push(symbol);
              arr.push(objbids);
              arr.push(objasks);
              this.redispubkeep.publish('depthcachesymbol', JSON.stringify(arr));
            });
          }

          if (counter === 5000) {
            console.log('Postgre check generall connect. Counter Ticks:', counter);
            counter = 0;
            const sql = 'SELECT * FROM public.monitoringsbinance_mar_total_sim LIMIT 1;';
            // this.postgre.query(sql, () => {});
            this.postgreone.query(sql, (err) => {
              if (err) console.log(err);
            });
          }
        });
      }
    }, this.depthCacheBinance);
  }

  async recentTrades(symbol) {
    return new Promise((resolve, reject) => {
      const map = new Map();
      this.binance.history.recentTrades(symbol, (error, trades) => {
        if (error) reject(error);

        // console.log(symbol, trades.length);

        if (trades.length !== 0 && Array.isArray(trades)) {
          const dateNow1 = new Date();
          this.redis.pipeline([
            ['set', `${symbol}:trades`, JSON.stringify(trades)],
          ]).exec(() => { });

          trades.forEach((dataset) => {
            const diff1 = (parseFloat(dateNow1.getTime()) - parseFloat(dataset.time)) / 1000;
            if (diff1 <= 60.0 && !dataset.isBuyerMaker) {
              // console.log(symbol, diff1, dataset.price, dataset.qty, dataset.isBuyerMaker);
              let qty = 0.0;
              const price = parseFloat(dataset.price);
              if (map.has(price)) {
                qty = map.get(price);
                map.set(price, qty + parseFloat(dataset.qty));
              } else {
                map.set(price, parseFloat(dataset.qty));
              }
            } else {
              // let mapMarket = new Map();
              // mapMarket.set(symbol, map);
              resolve(map);
            }
          });
        } else {
          const errorTradesLen = {
            status: 'error trades length',
            market: symbol,
          };

          reject(errorTradesLen);
        }
      }, 100);
    });
  }

  async setBuySellNow(symbol, bids, asks) {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${symbol}:bids`, JSON.stringify(bids)],
        ['set', `${symbol}:asks`, JSON.stringify(asks)],
      ]).exec((err) => {
        if (err) {
          console.log(err);
          reject(err);
        }

        resolve(true);
      });
    });
  }
}

module.exports = Binancekeeping;