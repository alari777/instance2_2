class Preparation {
  constructor(redis, postgre) {
    this.redis = redis;
    this.postgre = postgre;

    this.container = '1';
    this.marketFulls = {};
    this.marketNames = {};

    this.delistMarkets = ['BNBBTC', 'CLOAKBTC', 'MODBTC', 'SALTBTC', 'SUBBTC', 'WINGSBTC', 'NULSBTC', 'ENJBTC', 'EVXBTC', 'VIBBTC', 'WTCBTC'];

    return this;
  }

  async getMarketsForWork() {
    try {
      const marketsObj = await this.getMarketsFromPostgreDB();
      if (marketsObj === null) return null;

      this.marketFulls = marketsObj.f;
      this.marketNames = marketsObj.n;

      const resultStr = await this.keepDatasForWork();
      return {
        step1: marketsObj,
        step2: resultStr,
      };
    } catch (err) {
      return err;
    }
  }

  getMarketsFromPostgreDB() {
    return new Promise((resolve, reject) => {
      let sql = '';

      if (process.env.ROBOT_MODE === 'dev') {
        // switch (process.env.MONITORINGSBINANCE_TEST1) {
        //   case 'monitoringsbinance_mar_total_sim_test3': // EXPEREMENTAL
        //     // sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} WHERE (container = $1 AND market IN ('BNBBTC', 'ETHBTC', 'IOTABTC', 'BCHABCBTC', 'XRPBTC', 'EOSBTC', 'ADABTC', 'LTCBTC', 'NEOBTC', 'BATBTC', 'RVNBTC', 'TRXBTC', 'NANOBTC', 'IOSTBTC', 'ONTBTC', 'XMRBTC', 'XLMBTC', 'ZECBTC', 'WAVESBTC', 'DASHBTC', 'QTUMBTC', 'OMGBTC', 'VETBTC'));`;
        //     sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} WHERE ( container = $1 AND market NOT IN ('BTCUSDT', 'TUSDBTC', 'BCNBTC', 'TRIGBTC', 'ICNBTC', 'CHATBTC', 'BCCBTC') ) ${process.env.POSTGRE_RANGE_COINS};`;
        //     break;
        //   case 'monitoringsbinance_mar_total_sim_test5': // USDT
        //     sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} WHERE (container = $1);`;
        //     break;
        //   // case 'monitoringsbinance_mar_total_sim_test6':
        //   //   sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} WHERE (container = $1 AND market IN ('BNBBTC', 'ETHBTC', 'IOTABTC', 'BCHABCBTC', 'XRPBTC', 'EOSBTC', 'ADABTC', 'LTCBTC', 'NEOBTC', 'BATBTC', 'RVNBTC', 'TRXBTC', 'NANOBTC', 'IOSTBTC', 'ONTBTC', 'XMRBTC', 'XLMBTC', 'ZECBTC', 'WAVESBTC', 'DASHBTC', 'QTUMBTC', 'OMGBTC', 'VETBTC'));`;
        //   //   break;
        //   default:
        //     sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} WHERE ( container = $1 AND market NOT IN ('BTCUSDT', 'TUSDBTC', 'BCNBTC', 'TRIGBTC', 'ICNBTC', 'CHATBTC', 'BCCBTC') ) ${process.env.POSTGRE_RANGE_COINS};`;
        //     break;
        // }

        if (process.env.MONITORINGSBINANCE_TEST1 !== 'monitoringsbinance_mar_total_sim_test5') {
          sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} WHERE ( container = $1 AND market NOT IN ('BTCUSDT', 'TUSDBTC', 'BCNBTC', 'TRIGBTC', 'ICNBTC', 'CHATBTC', 'BCCBTC') ) ${process.env.POSTGRE_RANGE_COINS};`;
        } else {
          sql = `SELECT * FROM public.${process.env.MONITORINGSBINANCE_TEST1} WHERE (container = $1);`;
        }
      }

      if (process.env.ROBOT_MODE === 'prod') {
        sql = `SELECT * FROM public.monitoringsbinance_test1 WHERE ( container = $1 AND market NOT IN ('BTCUSDT', 'TUSDBTC', 'BCNBTC', 'TRIGBTC', 'ICNBTC', 'CHATBTC', 'BCCBTC') ) ${process.env.POSTGRE_RANGE_COINS};`;
      }

      if (sql !== '') {
        this.postgre.query(sql, [this.container], (err, result) => {
          if (err) reject(err);
          let counter = 0;
          const marketFulls = [];
          const marketNames = [];
          if (result.rowCount >= 1) {
            result.rows.forEach((dataset) => {
              if (!this.delistMarkets.includes(dataset.market)) {
                marketFulls.push(dataset);
                marketNames.push(dataset.market);
              }
              counter += 1;
              if (result.rowCount === counter) {
                counter = 0;
                const obj = {
                  f: marketFulls,
                  n: marketNames,
                };
                resolve(obj);
              }
            });
          } else {
            resolve(null);
          }
        });
      } else {
        resolve(null);
      }
    });
  }

  keepDatasForWork() {
    return new Promise((resolve, reject) => {
      let counterYes = 0;
      const counterNo = 0;
      const marketslen = this.marketFulls.length;

      this.marketFulls.forEach((dataset) => {
        if (dataset.typetrd === 'buy' || dataset.typetrd === 'sell') {
          let additionalDataForMarket = 0;
          let n = dataset.additionaldata;
          n = parseFloat(n);
          n = (typeof n === 'string') ? n : n.toString();
          if (n.indexOf('e') !== -1) {
            additionalDataForMarket = parseInt(n.split('e')[1], 0) * -1;
          } else {
            const separator = (1.1).toString().split('1')[1];
            const parts = n.split(separator);
            const t = parts.length > 1 ? parts[parts.length - 1].length : 0;
            additionalDataForMarket = t;
          }

          this.redis.pipeline([
            ['set', `${dataset.market}:buysellnow`, 0.0],
            ['set', `${dataset.market}:amount`, dataset.amount],
            ['set', `${dataset.market}:deep`, dataset.deep],
            ['set', `${dataset.market}:typetrd`, dataset.typetrd],
            ['set', `${dataset.market}:thiswallpricesell`, 0.0],
            ['set', `${dataset.market}:thiswallpricebuy`, 0.0],
            ['set', `${dataset.market}:thiswallvolumebuy`, 0.0],
            ['set', `${dataset.market}:lastwallprice`, 0.0],
            ['set', `${dataset.market}:ratebuy`, dataset.ratebuy],
            ['set', `${dataset.market}:spread`, dataset.note],
            ['set', `${dataset.market}:additionaldata`, dataset.additionaldata],
            ['set', `${dataset.market}:stoploss`, dataset.stoploss],
            ['set', `${dataset.market}:raz`, additionalDataForMarket],
          ]).exec((err) => {
            if (err) reject(err);

            counterYes += 1;
            if (marketslen === (counterYes + counterNo)) {
              resolve('All markets keeping in redis.');
            }
          });
        } else counterYes += 1;
      });
    });
  }
}

module.exports = Preparation;