// Telegramm
const { TelegramClient } = require('messaging-api-telegram');

const clientTeleg = TelegramClient.connect(process.env.TELEGRAMM_CONNECT);

const settingsSell = require('./setting-sell.json');

class Algosell {
  constructor(connection, binancecalcs, symbol) {
    this.redis = connection.redisclients.redismain;

    this.sellsettings = settingsSell;
    this.market = symbol;

    this.fixPriceSell = 0;
    this.lastPriceSell = 0;
    this.amount = 0;
    this.rateBuyBuy = 0;
    this.stoploss = 0;
    this.raz = 0;
    this.stepsize = 0;

    this.binancecalcs = binancecalcs;
    this.datasBids = binancecalcs.datasBids;
    this.datasAsks = binancecalcs.datasAsks;

    return this;
  }

  async mainNewAlgo() {
    await this.setBuySellNow();
    const getBuyDatas = await this.getSellDatas();
    const arg11 = parseFloat(process.env.SELL_SIZE_WALL_1);
    const depthBuy90 = await this.binancecalcs.calcWall(arg11); // 60
    const arg12 = parseFloat(process.env.SELL_SIZE_SECONDWALL_1);
    const depthBuy90Second = await this.binancecalcs.calcWallSecond(arg12); // 60
    const arg21 = parseFloat(process.env.SELL_SIZE_FORWALL_1);
    const arg22 = parseFloat(process.env.SELL_SIZE_FORWALL_2);
    const depthAsks40Plus1 = await this.binancecalcs.calcForWall(arg21, arg22); // 40, 1
    const arg23 = parseFloat(process.env.SELL_SIZE_FORWALL_3);
    const arg24 = parseFloat(process.env.SELL_SIZE_FORWALL_4);
    const depthAsks40Plus21 = await this.binancecalcs.calcForWall(arg23, arg24); // 40, 21

    let bidsPosition = 0;
    if (process.env.ROBOT_MODE === 'dev') {
      bidsPosition = 11; // 21;
    }

    if (process.env.ROBOT_MODE === 'prod') {
      bidsPosition = 11; // 21;
    }

    if (getBuyDatas === null && bidsPosition !== 0) {
      if (process.env.TAKE_PROFIT === 'yes') {
        if (parseFloat(this.datasAsks[4].Rate) >= depthBuy90.Rate
            && depthBuy90.Quantity * 0.1 > depthBuy90Second.Quantity) {
          console.log('SELL FIRST.');
          return ('sell now');
        }
      }

      if (this.FixPriceSell === 0.0) {
        this.FixPriceSell = this.Stoploss;

        await this.setFixDatas();
      } else if (
        this.FixPriceSell < depthAsks40Plus1.Rate
          && depthAsks40Plus1.Rate < parseFloat(this.datasBids[bidsPosition].Rate)
      ) {
        this.FixPriceSell = depthAsks40Plus1.Rate;
        // console.log(this.market, depthAsks40Plus1, depthAsks40Plus21);

        await this.setFixDatas();

        if (process.env.ROBOT_MODE === 'dev') {
          const trailng = {};
          trailng.Symbol = this.market;
          trailng.Fixprice = this.FixPriceSell;

          const strTeleMsg = JSON.stringify(trailng);
          Algosell.sendTrailingViaTelegramm(strTeleMsg);
        }
      } else {
        if (this.FixPriceSell >= parseFloat(this.datasBids[0].Rate)) {
          console.log('SELL SECOND.');
          return ('sell now');
        }

        return ('sell: nothing do');
      }
    } else return ('sell: nothing do');

    return ('sell: nothing do');
  }

  async main() {
    await this.setBuySellNow();
    const getBuyDatas = await this.getSellDatas();
    const arg11 = parseFloat(process.env.SELL_SIZE_WALL_1);
    const depthBuy90 = await this.binancecalcs.calcWall(arg11); // 60
    const arg12 = parseFloat(process.env.SELL_SIZE_SECONDWALL_1);
    const depthBuy90Second = await this.binancecalcs.calcWallSecond(arg12); // 60
    const arg21 = parseFloat(process.env.SELL_SIZE_FORWALL_1);
    const arg22 = parseFloat(process.env.SELL_SIZE_FORWALL_2);
    const depthAsks40Plus1 = await this.binancecalcs.calcForWall(arg21, arg22); // 40, 1
    const arg23 = parseFloat(process.env.SELL_SIZE_FORWALL_3);
    const arg24 = parseFloat(process.env.SELL_SIZE_FORWALL_4);
    const depthAsks40Plus21 = await this.binancecalcs.calcForWall(arg23, arg24); // 40, 21

    let bidsPosition = 0;
    if (process.env.ROBOT_MODE === 'dev') {
      bidsPosition = 11; // 21;
    }

    if (process.env.ROBOT_MODE === 'prod') {
      bidsPosition = 11; // 21;
    }

    if (getBuyDatas === null && bidsPosition !== 0) {
      if (process.env.TAKE_PROFIT === 'yes') {
        if (parseFloat(this.datasAsks[4].Rate) >= depthBuy90.Rate
            && depthBuy90.Quantity * 0.1 > depthBuy90Second.Quantity) {
          console.log('SELL FIRST.');
          return ('sell now');
        }
      }

      if (this.FixPriceSell === 0.0) {
        this.FixPriceSell = this.Stoploss;

        await this.setFixDatas();
      } else if (
        this.FixPriceSell < depthAsks40Plus1.Rate
          && depthAsks40Plus1.Rate < parseFloat(this.datasBids[bidsPosition].Rate)
          && this.RateBuyBuy < parseFloat(depthAsks40Plus21.Rate)
      ) {
        this.FixPriceSell = depthAsks40Plus1.Rate;
        // console.log(this.market, depthAsks40Plus1, depthAsks40Plus21);

        await this.setFixDatas();

        if (process.env.ROBOT_MODE === 'dev') {
          const trailng = {};
          trailng.Symbol = this.market;
          trailng.Fixprice = this.FixPriceSell;

          const strTeleMsg = JSON.stringify(trailng);
          Algosell.sendTrailingViaTelegramm(strTeleMsg);
        }
      } else {
        if (this.FixPriceSell >= parseFloat(this.datasBids[0].Rate)) {
          console.log('SELL SECOND.');
          return ('sell now');
        }

        return ('sell: nothing do');
      }
    } else return ('sell: nothing do');

    return ('sell: nothing do');
  }

  static sendTrailingViaTelegramm(message) {
    const trailing = JSON.parse(message);
    const symbol = trailing.Symbol.replace('BTC', '-BTC');
    const stoploss = trailing.Fixprice.toFixed(8);

    let msgTrial = `🔔 ${process.env.NAME_OF_TEST_BOT}: ${symbol}`;
    msgTrial += `\n💸 MOVE STOP-LOSS TO: ${stoploss}`;

    let msgSub = `🔔 ${process.env.NAME_OF_TEST_BOT}: ${symbol}`;
    msgSub += `\n💸 MOVE STOP-LOSS TO: ${stoploss}`;

    clientTeleg.sendMessage(142545448, msgTrial, {
      disable_web_page_preview: true,
      disable_notification: true,
    });

    clientTeleg.sendMessage(159856577, msgSub, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
  }

  getSellDatas() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['get', `${this.market}:thiswallpricesell`],
        ['get', `${this.market}:lastwallprice`],
        ['get', `${this.market}:amount`],
        ['get', `${this.market}:ratebuy`],
        ['get', `${this.market}:stoploss`],
        ['get', `${this.market}:raz`],
        ['get', `${this.market}:additionaldata`],
      ]).exec((err, results) => {
        if (err) reject(err);

        this.FixPriceSell = parseFloat(results[0][1]);
        this.LastPriceSell = parseFloat(results[1][1]);
        this.Amount = parseFloat(results[2][1]);
        this.RateBuyBuy = parseFloat(results[3][1]);
        this.Stoploss = parseFloat(results[4][1]);
        this.Raz = parseFloat(results[5][1]);
        this.stepsize = results[6][1];

        resolve(null);
      });
    });
  }

  setFixDatas() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${this.market}:thiswallpricesell`, this.FixPriceSell],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  setBuySellNow() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${this.market}:buysellnow`, this.datasBids[0].Rate],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  get FixPriceSell() {
    return parseFloat(this.fixPriceSell);
  }

  set FixPriceSell(value) {
    this.fixPriceSell = parseFloat(value);
  }

  get LastPriceSell() {
    return parseFloat(this.lastPriceSell);
  }

  set LastPriceSell(value) {
    this.lastPriceSell = parseFloat(value);
  }

  get Amount() {
    return parseFloat(this.amount);
  }

  set Amount(value) {
    this.amount = parseFloat(value);
  }

  get RateBuyBuy() {
    return parseFloat(this.rateBuyBuy);
  }

  set RateBuyBuy(value) {
    this.rateBuyBuy = parseFloat(value);
  }

  get Stoploss() {
    return parseFloat(this.stoploss);
  }

  set Stoploss(value) {
    this.stoploss = parseFloat(value);
  }

  get Raz() {
    return parseFloat(this.raz);
  }

  set Raz(value) {
    this.raz = parseFloat(value);
  }
}

module.exports = Algosell;